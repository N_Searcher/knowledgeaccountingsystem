﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class KnowledgeArea : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Knowledge> Knowledges { get; set; }
    }
}
