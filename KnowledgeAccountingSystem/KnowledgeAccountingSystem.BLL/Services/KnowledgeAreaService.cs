﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Infrastructure;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class KnowledgeAreaService : IKnowledgeAreaService
    {
        private readonly IUnitOfWork _dbWork;
        private readonly IMapper _mapper;

        public KnowledgeAreaService(IUnitOfWork dbWork, IMapper mapper)
        {
            _dbWork = dbWork;
            _mapper = mapper;
        }

        public async Task AddAsync(KnowledgeAreaDTO knowledge)
        {
            var knowledgeCheck = await _dbWork?.KnowledgeAreaRepository?.ReadAsync(knowledge.AreaId);

            if (knowledgeCheck != null)
                throw new KnowledgeAccountingSystemException("Such knowledge area already exists");

            var knowledgeItem = _mapper?.Map<KnowledgeArea>(knowledge);
            await _dbWork?.KnowledgeAreaRepository?.AddAsync(knowledgeItem);
        }

        public void Dispose()
        {
            _dbWork.Dispose();
        }

        public IEnumerable<KnowledgeDTO> GetAllAreaKnowledges(int areaId)
        {
            var knowledges = _dbWork?.KnowledgeAreaRepository
                .ReadAllIncludingKnowledgesAsync()
                .Result?
                .SingleOrDefault(item => item.Id == areaId)
                .Knowledges?.AsEnumerable();

            if (knowledges is null)
                throw new KnowledgeAccountingSystemException("There is no such area");

            return _mapper?.Map<IEnumerable<KnowledgeDTO>>(knowledges);
        }

        public IEnumerable<KnowledgeAreaDTO> GetAllAreasWithKnowledges()
        {
            var areas = _dbWork?.KnowledgeAreaRepository?
                .ReadAllIncludingKnowledgesAsync();
            return _mapper?.Map<IEnumerable<KnowledgeAreaDTO>>(areas);
        }

        public async Task RemoveAsync(int id)
        {
            var area =  await _dbWork.KnowledgeAreaRepository.ReadAsync(id);

            if (area is null)
                throw new KnowledgeAccountingSystemException("There is nothing to delete, please provide correct ID");

            await _dbWork.KnowledgeAreaRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(KnowledgeAreaDTO knowledge)
        {
            var area = await _dbWork.KnowledgeAreaRepository.ReadAsync(knowledge.AreaId);

            if (area is null)
                throw new KnowledgeAccountingSystemException("There is nothing to update, please provide correct Knowledge Area");

            var knowledgeArea = _mapper.Map<KnowledgeArea>(knowledge);
            await _dbWork.KnowledgeAreaRepository.UpdateAsync(knowledgeArea);
        }
    }
}
