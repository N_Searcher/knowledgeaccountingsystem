﻿using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly KnowledgeAccountingDBContext _dBContext;
        private KnowledgeRepository _knowledgeRepository;
        private KnowledgeAreaRepository _knowledgeAreaRepository;
        private RatedKnowledgeRepository _ratedKnowledgeRepository;
        private bool _disposed = false;

        public UnitOfWork(KnowledgeAccountingDBContext context)
        {
            _dBContext = context;
        }

        public IKnowledgeRepository KnowledgeRepository
        {
            get
            {
                if (_knowledgeRepository == null)
                    _knowledgeRepository = new KnowledgeRepository(_dBContext);
                return _knowledgeRepository;
            }
        }

        public IKnowledgeAreaRepository KnowledgeAreaRepository
        {
            get
            {
                if (_knowledgeAreaRepository == null)
                    _knowledgeAreaRepository = new KnowledgeAreaRepository(_dBContext);
                return _knowledgeAreaRepository;
            }
        }

        public IRatedKnowledgeRepository RatedKnowledgeRepository
        {
            get
            {
                if (_ratedKnowledgeRepository == null)
                    _ratedKnowledgeRepository = new RatedKnowledgeRepository(_dBContext);
                return _ratedKnowledgeRepository;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _dBContext.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAsync()
        {
            await _dBContext.SaveChangesAsync();
        }
    }
}
