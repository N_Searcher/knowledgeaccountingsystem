﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class RatedKnowledge : BaseEntity
    {
        public int Rating { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("Knowledge")]
        public int KnowledgeId { get; set; }

        public virtual Knowledge Knowledge { get; set; }
    }
}
