﻿using KnowledgeAccountingSystem.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IKnowledgeService : IBaseService<KnowledgeDTO> { }
}
