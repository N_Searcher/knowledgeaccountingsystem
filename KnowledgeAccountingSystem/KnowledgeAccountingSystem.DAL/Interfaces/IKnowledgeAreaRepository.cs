﻿using KnowledgeAccountingSystem.DAL.Entities;
using System.Threading.Tasks;
using System.Linq;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IKnowledgeAreaRepository : IBaseRepository<KnowledgeArea>
    {
        Task<IQueryable<KnowledgeArea>> ReadAllIncludingKnowledgesAsync();
        Task AddKnowledgeToTheAreaAsync(int areaId, int knowledgeId);
        Task RemoveKnowledgeFromTheAreaAsync(int areaId, int knowledgeId);
    }
}
