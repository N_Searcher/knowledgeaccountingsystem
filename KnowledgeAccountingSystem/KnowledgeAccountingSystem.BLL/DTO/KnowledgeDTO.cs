﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class KnowledgeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public KnowledgeAreaDTO KnowledgeArea { get; set; }
    }
}
