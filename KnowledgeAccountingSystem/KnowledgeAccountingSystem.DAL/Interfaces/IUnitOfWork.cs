﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IKnowledgeRepository KnowledgeRepository { get; }
        IKnowledgeAreaRepository KnowledgeAreaRepository { get; }
        IRatedKnowledgeRepository RatedKnowledgeRepository { get; }

        Task SaveAsync();
    }
}
