﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IBaseService<T> where T : class
    {
        Task AddAsync(T knowledge);
        Task UpdateAsync(T knowledge);
        Task RemoveAsync(int id);
        void Dispose();
    }
}
