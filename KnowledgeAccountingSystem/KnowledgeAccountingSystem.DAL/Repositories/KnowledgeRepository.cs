﻿using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class KnowledgeRepository : IKnowledgeRepository
    {
        private readonly KnowledgeAccountingDBContext _dBContext;

        public KnowledgeRepository(KnowledgeAccountingDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task AddAsync(Knowledge entity)
        {
            await _dBContext.Knowledges.AddAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            var knowledge = _dBContext.Knowledges.Find(id);
            await Task.FromResult(_dBContext.Knowledges.Remove(knowledge));
        }

        public async Task<IQueryable<Knowledge>> ReadAllAsync()
        {
            return await Task.FromResult(_dBContext.Knowledges.AsQueryable());
        }

        public async Task<Knowledge> ReadAsync(int id)
        {
            return await _dBContext.Knowledges.FindAsync(id);
        }

        public async Task UpdateAsync(Knowledge entity)
        {
            await Task.FromResult(_dBContext.Knowledges.Update(entity));
        }
    }
}
