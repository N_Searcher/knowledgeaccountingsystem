﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class User : BaseEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public virtual ICollection<RatedKnowledge> RatedKnowledges { get; set; }
    }
}
