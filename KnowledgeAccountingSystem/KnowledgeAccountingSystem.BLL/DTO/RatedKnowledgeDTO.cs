﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class RatedKnowledgeDTO
    {
        public int Id { get; set; }
        public int Rating { get; set; }
        public KnowledgeDTO Knowledge { get; set; }
    }
}
