﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.Infrastructure
{
    [Serializable]
    public class KnowledgeAccountingSystemException : Exception
    {
        public KnowledgeAccountingSystemException()
        {
        }

        public KnowledgeAccountingSystemException(string message) : base(message)
        {
        }

        public KnowledgeAccountingSystemException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KnowledgeAccountingSystemException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
