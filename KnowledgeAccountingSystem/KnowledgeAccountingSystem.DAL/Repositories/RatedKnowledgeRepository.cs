﻿using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class RatedKnowledgeRepository : IRatedKnowledgeRepository
    {
        private readonly KnowledgeAccountingDBContext _dBContext;

        public RatedKnowledgeRepository(KnowledgeAccountingDBContext dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task AddAsync(RatedKnowledge entity)
        {
            await _dBContext.RatedKnowledges.AddAsync(entity);
        }

        public async Task DeleteAsync(int id)
        {
            var ratedKnowledge = _dBContext.RatedKnowledges.Where(item => item.Id == id)
                .SingleOrDefault();
            await Task.FromResult(_dBContext.RatedKnowledges.Remove(ratedKnowledge));
        }

        public async Task<IQueryable<RatedKnowledge>> ReadAllAsync()
        {
            return await Task.FromResult(_dBContext.RatedKnowledges.AsQueryable());
        }

        public async Task<RatedKnowledge> ReadAsync(int id)
        {
            var ratedKnowledge = _dBContext.RatedKnowledges.Where(item => item.Id == id)
                .SingleOrDefault();
            return await Task.FromResult(ratedKnowledge);
        }

        public async Task UpdateAsync(RatedKnowledge entity)
        {
            await Task.FromResult(_dBContext.RatedKnowledges.Update(entity));
        }
    }
}
