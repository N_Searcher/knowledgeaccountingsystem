﻿using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace KnowledgeAccountingSystem.DAL
{
    public class AuthOptions
    {
        public const string ISSUER = "TokenPublisher";
        public const string AUDIENCE = "KnowledgeAccountingSystem";
        const string KEY = "LICnj3e8f7hno8y#";
        public const int LIFETIME = 5;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
