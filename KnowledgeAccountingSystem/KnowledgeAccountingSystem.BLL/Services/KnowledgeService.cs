﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using KnowledgeAccountingSystem.DAL.Entities;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Infrastructure;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class KnowledgeService : IKnowledgeService
    {
        private readonly IUnitOfWork _dbWork;
        private readonly IMapper _mapper;

        public KnowledgeService(IUnitOfWork dbWork, IMapper mapper)
        {
            _dbWork = dbWork;
            _mapper = mapper;
        }

        public async Task AddAsync(KnowledgeDTO knowledge)
        {
            var knowledgeCheck = await _dbWork?.KnowledgeRepository?.ReadAsync(knowledge.Id);
            if (knowledgeCheck != null)
                throw new KnowledgeAccountingSystemException("Such knowledge already exists");
            var mappedKnowledge = _mapper?.Map<Knowledge>(knowledge);
            await _dbWork.KnowledgeRepository.AddAsync(mappedKnowledge);
        }

        public void Dispose()
        {
            _dbWork.Dispose();
        }

        public async Task RemoveAsync(int id)
        {
            var knowledge = await _dbWork?.KnowledgeRepository?.ReadAsync(id);
            if (knowledge is null)
                throw new KnowledgeAccountingSystemException("There is no such knowledge");
            await _dbWork.KnowledgeRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(KnowledgeDTO knowledge)
        {
            var knowledgeCheck = await _dbWork?.KnowledgeRepository?.ReadAsync(knowledge.Id);
            if (knowledgeCheck is null)
                throw new KnowledgeAccountingSystemException("There is no such knowledge");
            var mappedKnowledge = _mapper?.Map<Knowledge>(knowledge);
            await _dbWork.KnowledgeRepository.AddAsync(mappedKnowledge);
        }
    }
}
