﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.Infrastructure
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Knowledge, KnowledgeDTO>().ReverseMap();
            CreateMap<RatedKnowledge, RatedKnowledgeDTO>().ReverseMap();
            CreateMap<KnowledgeArea, KnowledgeAreaDTO>()
                .ForMember(dest => dest.AreaName, src => src.MapFrom(item => item.Name))
                .ForMember(dest => dest.AreaId, src => src.MapFrom(item => item.Id))
                .ReverseMap();
        }
    }
}
