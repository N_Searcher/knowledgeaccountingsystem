﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Infrastructure;
using KnowledgeAccountingSystem.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystemPL.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class KnowledgeAreaController : ControllerBase
    {
        private readonly IKnowledgeAreaService _areaService;

        public KnowledgeAreaController(IKnowledgeAreaService areaService)
        {
            _areaService = areaService;
        }

        [HttpGet("knowledges")]
        public ActionResult<IEnumerable<KnowledgeAreaDTO>> GetAllAreasWithKnowledges()
        {
            try
            {
                var result = _areaService.GetAllAreasWithKnowledges();
                return Ok(result);
            }
            catch (KnowledgeAccountingSystemException)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/knowledges")]
        public ActionResult<IEnumerable<KnowledgeDTO>> GetKnowledgesRelatedToTheArea(int areaId)
        {
            try
            {
                var result = _areaService.GetAllAreaKnowledges(areaId);
                return Ok(result);
            }
            catch (KnowledgeAccountingSystemException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddNewArea([FromBody] KnowledgeAreaDTO areaDTO)
        {
            try
            {
                await _areaService.AddAsync(areaDTO);
                return StatusCode(201);
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<ActionResult> ChangeArea([FromBody] KnowledgeAreaDTO areaDTO)
        {
            try
            {
                await _areaService.UpdateAsync(areaDTO);
                return Ok();
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> RemoveArea(int id)
        {
            try
            {
                await _areaService.RemoveAsync(id);
                return Ok();
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }
    }
}
