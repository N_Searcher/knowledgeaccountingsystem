﻿using KnowledgeAccountingSystem.DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task AddAsync(T entity);
        Task<T> ReadAsync(int id);
        Task<IQueryable<T>> ReadAllAsync();
        Task UpdateAsync(T entity);
        Task DeleteAsync(int id);
    }
}
