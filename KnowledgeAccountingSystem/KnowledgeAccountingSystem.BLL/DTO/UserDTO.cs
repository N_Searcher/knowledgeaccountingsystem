﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class UserDTO
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Damn it! Incorrect data")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password mf")]
        public string PasswordConfirmation { get; set; }
    }
}
