﻿using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace KnowledgeAccountingSystem.BLL.DTO
{
    public class KnowledgeAreaDTO
    {
        public int AreaId { get; set; }
        public int AreaName { get; set; }
        public ICollection<KnowledgeDTO> Knowledges { get; set; }
    }
}
