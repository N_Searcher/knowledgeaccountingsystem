﻿using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class KnowledgeAreaRepository : IKnowledgeAreaRepository
    {
        private readonly KnowledgeAccountingDBContext _dBContext;

        public KnowledgeAreaRepository(KnowledgeAccountingDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task AddAsync(KnowledgeArea entity)
        {
            await _dBContext.KnowledgeAreas.AddAsync(entity);
        }

        public async Task AddKnowledgeToTheAreaAsync(int areaId, int knowledgeId)
        {
            var area = await _dBContext.KnowledgeAreas.FindAsync(areaId);
            var knowledge = await _dBContext.Knowledges.FindAsync(knowledgeId);
            area.Knowledges.Add(knowledge);
        }

        public async Task DeleteAsync(int id)
        {
            var knowledgeArea = _dBContext.KnowledgeAreas.Find(id);
            await Task.FromResult(_dBContext.KnowledgeAreas.Remove(knowledgeArea));
        }

        public async Task<IQueryable<KnowledgeArea>> ReadAllAsync()
        {
            return await Task.FromResult(_dBContext.KnowledgeAreas.AsNoTracking().AsQueryable());
        }

        public async Task<IQueryable<KnowledgeArea>> ReadAllIncludingKnowledgesAsync()
        {
            return await Task.FromResult(_dBContext.KnowledgeAreas.AsNoTracking().Include(item => item.Knowledges).AsQueryable());
        }

        public async Task<KnowledgeArea> ReadAsync(int id)
        {
            return await _dBContext.KnowledgeAreas.AsNoTracking().Where(item => item.Id == id).SingleOrDefaultAsync();
        }

        public async Task RemoveKnowledgeFromTheAreaAsync(int areaId, int knowledgeId)
        {
            var area = await _dBContext.KnowledgeAreas.FindAsync(areaId);
            var knowledge = await _dBContext.Knowledges.FindAsync(knowledgeId);
            area.Knowledges.Remove(knowledge);
        }

        public async Task UpdateAsync(KnowledgeArea entity)
        {
            await Task.FromResult(_dBContext.KnowledgeAreas.Update(entity));
        }
    }
}
