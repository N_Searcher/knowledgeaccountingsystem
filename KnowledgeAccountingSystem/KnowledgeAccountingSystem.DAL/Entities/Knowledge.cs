﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Knowledge : BaseEntity
    {
        public string Name { get; set; }

        [ForeignKey("KnowledgeArea")]
        public int AreaId { get; set; }
        public virtual KnowledgeArea KnowledgeArea { get; set; }
    }
}
