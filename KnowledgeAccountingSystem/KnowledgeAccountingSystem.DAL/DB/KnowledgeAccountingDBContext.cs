﻿using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace KnowledgeAccountingSystem.DAL
{
    public class KnowledgeAccountingDBContext : DbContext
    {
        public DbSet<Knowledge> Knowledges { get; set; }
        public DbSet<KnowledgeArea> KnowledgeAreas { get; set; }
        public DbSet<RatedKnowledge> RatedKnowledges { get; set; }

        public KnowledgeAccountingDBContext() : base(){}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Database=KnowledgeAccountingSystemDB;");
        }
    }
}
