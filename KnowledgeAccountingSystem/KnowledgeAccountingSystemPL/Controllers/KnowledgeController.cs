﻿using KnowledgeAccountingSystem.BLL.DTO;
using KnowledgeAccountingSystem.BLL.Infrastructure;
using KnowledgeAccountingSystem.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystemPL.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class KnowledgeController : ControllerBase
    {
        private readonly IKnowledgeService _knowledgeService;

        public KnowledgeController(IKnowledgeService knowledgeService)
        {
            _knowledgeService = knowledgeService;
        }

        [HttpPost]
        public async Task<ActionResult> AddKnowledge([FromBody] KnowledgeDTO knowledgeDTO)
        {
            try
            {
                await _knowledgeService.AddAsync(knowledgeDTO);
                return StatusCode(201);
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<ActionResult> UpdateKnowledge([FromBody] KnowledgeDTO knowledgeDTO)
        {
            try
            {
                await _knowledgeService.UpdateAsync(knowledgeDTO);
                return Ok();
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> RemoveKnowledge(int id)
        {
            try
            {
                await _knowledgeService.RemoveAsync(id);
                return Ok();
            }
            catch (KnowledgeAccountingSystemException)
            {
                return BadRequest();
            }
        }
    }
}
