﻿using KnowledgeAccountingSystem.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IKnowledgeRepository : IBaseRepository<Knowledge>{}
}
